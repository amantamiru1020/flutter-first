import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  MyButton(this.title, {required this.onTap, Key? key}) : super(key: key);
  String title;
  Function() onTap;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Colors.blue,
            boxShadow: [BoxShadow(color: Colors.grey, offset: Offset(2, 3))]),
        child: Text(
          title,
          style: TextStyle(
              color: Colors.yellow, fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
    );
  }
}
