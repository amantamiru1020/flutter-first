import 'package:flutter/material.dart';

class MyLabeledTextfield extends StatelessWidget {
  MyLabeledTextfield({required this.label, this.show = false, Key? key})
      : super(key: key);
  String label;
  bool show;
  TextEditingController controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(label),
        Expanded(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 40),
            decoration: BoxDecoration(color: Colors.yellowAccent),
            child: TextField(
              controller: controller,
              obscureText: show,
            ),
          ),
        ),
      ],
    );
  }
}
