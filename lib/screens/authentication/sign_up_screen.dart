import 'package:flutter/material.dart';
import 'package:flutter_application_1/widgets/my_button.dart';
import 'package:flutter_application_1/widgets/my_labeled_textfield.dart';

class SignUpScreen extends StatelessWidget {
  SignUpScreen({Key? key}) : super(key: key);

  MyLabeledTextfield first = MyLabeledTextfield(label: "User name");
  MyLabeledTextfield second = MyLabeledTextfield(
    label: "Password",
    show: true,
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            SizedBox(
              height: 100,
            ),
            first,
            second,
            MyButton(
              "SignUp",
              onTap: () {
                print(first.controller.text + " and " + second.controller.text);
              },
            ),
          ],
        ),
      ),
    );
  }
}
